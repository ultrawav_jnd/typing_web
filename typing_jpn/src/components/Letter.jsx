import React, { useState } from "react";

function Letter(props) {
  let letter_type = props.type;

  let letterState = letter_type;

  if (props.good === "g") {
    letterState = letter_type + " good";
  } else if (props.good === "b") {
    letterState = letter_type + " bad";
  }

  console.log(props.letter + "=" + letterState);

  return (
    <div className={letterState}>
      {letterState[0] === "l" ? (
        <h5 className="sound">{props.sound}</h5>
      ) : null}
      <span>{props.letter}</span>
    </div>
  );
}

export default Letter;
