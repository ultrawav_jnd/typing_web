import React, { useState } from "react";
import Header from "./Header";
import Typing from "./Typing";
import '../css/App.css';
import { getLesson, getLessonMaxLv, toRomaji } from "../LessonData";
import { Link, Route, Switch, useParams } from "react-router-dom";

function Main(props) {
  let { mission_id } = useParams();
  const [lessonLv, setLessonLv] = useState(mission_id-1);
  
  const currentLesson = getLesson(lessonLv);

  function setNextLessonLv() {
    if (lessonLv === getLessonMaxLv()) {
      setLessonLv(0);
    } else {
      setLessonLv((oldLv) => {
        return oldLv + 1;
      });
    }
  }

  return (
    <div>
    <Header title={currentLesson.title}/>
    <Typing lesson={currentLesson} onNextLevel={setNextLessonLv} />
    </div>
  );
}

export default Main;
