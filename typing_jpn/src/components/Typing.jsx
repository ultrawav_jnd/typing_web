import React, { useState } from "react";
import Words from "./Words";
import GuidKeyboard from "./GuideKeyboard";
import { getLesson, getLessonMaxLv, toRomaji } from "../LessonData";

let keyPressSound = new Audio("/sounds/keypress0.mp3");

function isAlphabet(str) {
  return /^[a-zA-Z]$/.test(str);
}

function Typing(props) {
  const [step, setStep] = useState(0);
  const [typing, setTyping] = useState("");

  const lesson = props.lesson;
  const line = lesson.lines[step];
  const maxStep = lesson.lines.length - 1;
  console.log(line);

  function doKeyUp(event) {
    let newValue = event.target.value;
    let last = newValue.charAt(newValue.length - 1);

    keyPressSound.cloneNode().play();

    if (!isAlphabet(last)) {
      setTyping(newValue);
    }

    if (event.key === "Enter") {
      if (newValue.length >= line.length) {
        if (step === maxStep) {
          setStep(0);
          props.onNextLevel();
        } else {
          setStep((oldStep) => {
            return oldStep + 1;
          });
        }

        event.target.value = "";
        setTyping("");
      }
    }
  }

  function doSubmit(event) {
    event.preventDefault();
  }

  function getCursorIndex() {
    let cursor = typing.length / lesson.letter_count;
    cursor = Math.floor(cursor)
    console.log("cursor:" + cursor + " typing.length:" + typing.length);

    if (cursor >= line.length) {
      return undefined;
    }

    return cursor;
  }

  // 커서에 해당하는 문자수가 1개 이상일 수 있다.
  function getCursorLetters() {
    let cursorIndex = getCursorIndex();

    if (cursorIndex !== undefined) {
      let begin = cursorIndex * lesson.letter_count;
      let end = begin + lesson.letter_count;
      let each = line.substring(begin, end);
      console.log("each:" + each);
      return each;
    }
    return "";
  }

  function currentRomaji() {
    let cursorIndex = getCursorIndex();
    console.log("cursorIndex:" + cursorIndex);

    if (cursorIndex === undefined) {
      return "";
    }

    let cursorLetters = getCursorLetters();
    var romaji = toRomaji(cursorLetters);
    if (romaji === undefined) {
      console.log("undefined cursorLetters:" + cursorLetters);
      return "";
    }
    console.log("romaji:" + romaji);

    romaji = romaji.toUpperCase().split("").join(" ");
    console.log("romaji:" + romaji);

    return romaji;
  }

  return (
    <div>
      {/* <h2 className="highlight">text is super cool!</h2> */}

      <form className="words" onSubmit={doSubmit}>
        <Words
          words={line}
          letter_count={lesson.letter_count}
          typing={typing}
          type={lesson.type}
        />
        <input
          // className="user-input"
          className="words input highlight"
          onKeyUp={doKeyUp}
          name="keyboard"
          autoComplete="off"
          placeholder=""
          autoFocus
          // onCompositionUpdate={onUpdate}
          // value={typing}
        />
      </form>
      <GuidKeyboard keys={currentRomaji()} />
    </div>
  );
}

export default Typing;
