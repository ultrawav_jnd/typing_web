import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
// import Button from "@material-ui/core/Button";
import HomeIcon from "@material-ui/icons/Home";
import { Tooltip, IconButton } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
// local
import "../css/Home.css";
import MissionCard from "./MissionCard";
import MissionHeader from "./MissionHeader";
import lessonSentences from "../LessonData";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
  paper: {
    padding: theme.spacing(2),
    // textAlign: "center",
    // justify: "center",
    color: theme.palette.text.secondary,
  },
}));

function MissionList() {
  const thisStyle = useStyles();

  const gridCards = lessonSentences.map((value, index) => (
    <Grid item xs={12} sm={6} md={4} key={index}>
      <MissionCard
        id={index + 1}
        title={value.title}
        subtitle={value.lines[0]}
      />
    </Grid>
  ));

  return (
    <div>
      <MissionHeader /> 
      <div className="card-grid">
        <h2> Basic </h2>
        <Grid
          container
          spacing={4}
          // className = {thisStyle.gridContainer}
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          {gridCards}
        </Grid>
      </div>
    </div>
  );
}

export default MissionList;
