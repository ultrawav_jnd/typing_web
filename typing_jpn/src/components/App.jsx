import React from "react";
import Main from "./Main";
import Home from "./Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MissionList from "./MissionList";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/main/:mission_id">
          <Main />
        </Route>
        <Route path="/missions">
          <MissionList />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
