import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import "../css/Home.css";

function Home() {
  return (
    <div className="home-center">
      <h1>タイピング</h1>
      <br />
      <h2>Typing Nihongo for free!</h2>
      <br />
      <Link to="/missions" style={{ textDecoration: 'none' }}>
        <Button variant="contained" color="primary">
          GO TYPING
        </Button>
      </Link>
    </div>
  );
}

export default Home;
